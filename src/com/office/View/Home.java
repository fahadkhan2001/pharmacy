package com.office.View;

import com.office.View.Admin.Admin;

import javax.swing.*;
import java.awt.*;

public class Home extends Frame{

    public JFrame home_frame;
    JButton orderMedicine , viewSale,viewMedicine,exit;


    public Home(){

        home_frame = new JFrame("Pharmacy Management System");
        admin_login(home_frame);
        orderMedicine = new JButton("Order Medicine");
        viewMedicine = new JButton("Medicine Listing");
        viewSale = new JButton("View Sale");
        exit = new JButton("Exit");


        orderMedicine.setBounds(500, 350, 300, 40);
        viewMedicine.setBounds(500, 400, 300, 40);
        viewSale.setBounds(500, 450, 300, 40);
        exit.setBounds(500, 500, 300, 40);


        home_frame.add(orderMedicine);
        home_frame.add(viewMedicine);
        home_frame.add(viewSale);
        home_frame.add(exit);


        home_frame.setSize(1400, 1400);
        home_frame.setLayout(null);
        home_frame.setVisible(true);



        JMenuBar menu_bar;
        JMenu file, edit, about;
        JMenuItem cut, copy, paste, selectAll;
        home_frame.getJMenuBar();



//                                                              declaration Menu bar
        cut = new JMenuItem("Cut");
        copy = new JMenuItem("Copy");
        paste = new JMenuItem("Paste");
        selectAll = new JMenuItem("Select All");

        file = new JMenu("File");
        edit = new JMenu("Edit");
        about = new JMenu("About");
//
        edit.add(cut);edit.add(copy);edit.add(paste);edit.add(selectAll);
        menu_bar = new JMenuBar();
        menu_bar.add(file);menu_bar.add(edit);menu_bar.add(about);
        home_frame.add(menu_bar);
        home_frame.setJMenuBar(menu_bar);

//


        orderMedicine.setForeground(Color.BLACK);
        orderMedicine.setBackground(Color.ORANGE);
        viewMedicine.setBackground(Color.ORANGE);
        viewMedicine.setForeground(Color.BLACK);
        viewSale.setBackground(Color.ORANGE);
        viewSale.setForeground(Color.BLACK);
        exit.setBackground(Color.ORANGE);
        exit.setForeground(Color.BLACK);

//                                                                      OnClick Working.....



        workingOf_OrderMedicine(orderMedicine);
        workingOf_ViewMedicines(viewMedicine);
        workingOf_ViewSales(viewSale);
        workingOf_Exit(exit);
    }

    public void admin_login(Frame home_frame){
        JButton admin_login = new JButton("Admin Login");
        admin_login.setBackground(Color.ORANGE);
        admin_login.setForeground(Color.BLACK);
        admin_login.setBounds(500, 300, 300, 40);
        home_frame.add(admin_login);

        admin_login.addActionListener(el->{
            home_frame.dispose();
            Admin obj  = new Admin();
        });
    }

    public void workingOf_OrderMedicine(JButton orderMedicine){
        orderMedicine.addActionListener(el->{
            System.out.println("This one is from home");
        });

    }
    public void workingOf_ViewMedicines(JButton viewMedicine){
        viewMedicine.addActionListener(el->{
            System.out.println("View medicine for customers or low employess");
            View_Medicines view = new View_Medicines();
        });
    }

    public void workingOf_ViewSales(JButton viewSale){
        viewSale.addActionListener(el->{
            System.out.println("employees can view the sale only");
        });
    }

    public void workingOf_Exit(JButton exit){
        exit.addActionListener(el-> {
            System.exit(1);
        });
    }

    public static void main(String[] args) {

    }

}
