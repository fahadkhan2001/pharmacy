package com.office.Model;

public class AddProduct_model {

    private String p_name;
    private Integer P_id;
    private String p_varient;
    private Double p_price;
    private Integer p_quantity;

    public AddProduct_model(String p_name, Integer p_id, String p_varient, Double p_price, Integer p_quantity) {
        this.p_name = p_name;
        P_id = p_id;
        this.p_varient = p_varient;
        this.p_price = p_price;
        this.p_quantity = p_quantity;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public Integer getP_id() {
        return P_id;
    }

    public void setP_id(Integer p_id) {
        P_id = p_id;
    }

    public String getP_varient() {
        return p_varient;
    }

    public void setP_varient(String p_varient) {
        this.p_varient = p_varient;
    }

    public Double getP_price() {
        return p_price;
    }

    public void setP_price(Double p_price) {
        this.p_price = p_price;
    }

    public Integer getP_quantity() {
        return p_quantity;
    }

    public void setP_quantity(Integer p_quantity) {
        this.p_quantity = p_quantity;
    }

    @Override
    public String toString() {
        return "AddProduct_model{" +
                "p_name='" + p_name + '\'' +
                ", P_id=" + P_id +
                ", p_varient='" + p_varient + '\'' +
                ", p_price=" + p_price +
                ", p_quantity=" + p_quantity +
                '}';
    }
}
