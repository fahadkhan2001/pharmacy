package com.office.View.Admin;

import com.office.View.Admin.AddProduct;
import com.office.View.Admin.Admin;
import com.office.View.Home;
import javax.swing.*;
import java.awt.*;

public class AdminFunctionality_UI extends Home {

    JButton addProduct;

    @Override
    public void admin_login(Frame home_frame) { }

    @Override
    public void workingOf_OrderMedicine(JButton orderMedicine) {
        super.workingOf_OrderMedicine(orderMedicine);
    }

    @Override
    public void workingOf_ViewMedicines(JButton viewMedicine) {
        viewMedicine.addActionListener(el->{
            System.out.println("View medicine for admin he can delete and update any medicine");
        });
    }

    @Override
    public void workingOf_ViewSales(JButton viewSale) {
        viewSale.addActionListener(el->{
            System.out.println("Admin can view sale as well as update and delete the sale");
        });
    }

    @Override
    public void workingOf_Exit(JButton exit) {
        exit.addActionListener(el->{
            home_frame.dispose();
            Admin admin = new Admin();
        });
    }

    public AdminFunctionality_UI(){
        home_frame.setTitle("Admin Home Page");
        addProduct = new JButton("Add Product");
        addProduct.setBounds(500,300,300,40);
        addProduct.setForeground(Color.BLACK);
        addProduct.setBackground(Color.ORANGE);
        home_frame.add(addProduct);
        addProduct.addActionListener(el->{
            home_frame.dispose();
            AddProduct product = new AddProduct();
        });
    }
}
