package com.office.View.Admin;

import javax.swing.*;

public class AddProduct {

    //                                                                  Making frame.....
    JFrame addProduct_frame = new JFrame();
    JLabel p_id, p_name , p_category , p_variant, p_quantity , p_price;
    JTextField p_idText, p_nameText, p_categoryText, p_variantText,p_quantityText, p_priceText;
    JButton add_product , back_toAdminPage , medicineListing;

    public AddProduct(){

//                                                                   Making Inputs Fields.....
        p_id = new JLabel("Product ID : ");
        p_idText = new JTextField();

        p_name = new JLabel(" Name :");
        p_nameText = new JTextField();

        p_category = new JLabel("Product Category : ");
        p_categoryText = new JTextField();

        p_variant = new JLabel(" Product Varient : ");
        p_variantText = new JTextField();

        p_quantity = new JLabel("Product Quantity :");
        p_quantityText = new JTextField();

        p_price = new JLabel("Product Price : ");
        p_priceText = new JTextField();

//                                                                         Label size.....

        p_id.setBounds(50,60,100,30);
        p_name.setBounds(50,110,100,30);
        p_category.setBounds(50,160,220,30);
        p_variant.setBounds(50,210,220,30);
        p_quantity.setBounds(50,210,200,30);
        p_price.setBounds(50,260,100,30);

//                                                                          Text Field Size......
        p_idText.setBounds(160,60,250,30);
        p_nameText.setBounds(160,110,250,30);
        p_categoryText.setBounds(160,160,250,30);
        p_variantText.setBounds(160,160,250,30);
        p_quantityText.setBounds(160,210,250,30);
        p_priceText.setBounds(160,260,250,30);

//                                                                            Frame Buttons
        addProduct_frame.setTitle("Add Medicine");
        add_product = new JButton("Add Medicine");
        medicineListing = new JButton("Medicine Listing");
        back_toAdminPage = new JButton("Back to Admin Page");



//                                                                  Size of Buttons, inputs and frame....

        add_product.setBounds(50,400,120,40);
        medicineListing.setBounds(230,400,140,40);
        back_toAdminPage.setBounds(120,450,170,40);
        addProduct_frame.setSize(1400,1400);

//                                                                    Add Label AND TEXT FIELD ....
        addProduct_frame.add(p_id);
        addProduct_frame.add(p_idText);

        addProduct_frame.add(p_name);
        addProduct_frame.add(p_nameText);

        addProduct_frame.add(p_category);
        addProduct_frame.add(p_categoryText);

        addProduct_frame.add(p_variant);
        addProduct_frame.add(p_variantText);

        addProduct_frame.add(p_quantity);
        addProduct_frame.add(p_quantityText);

        addProduct_frame.add(p_price);
        addProduct_frame.add(p_priceText);

//                                                                      ADD BUTTONS.....

        addProduct_frame.add(add_product);
        addProduct_frame.add(medicineListing);
        addProduct_frame.add(back_toAdminPage);

        addProduct_frame.setLayout(null);
        addProduct_frame.setVisible(true);

        backTo_AdminPage(back_toAdminPage);

    }
    public void add_Product(JButton addProduct){

    }
    public void view_Medicines(JButton viewMedicine){

    }
    public void backTo_AdminPage(JButton back_ToAdmin){
        back_ToAdmin.addActionListener(el->{
            addProduct_frame.dispose();
            AdminFunctionality_UI functionality = new AdminFunctionality_UI();
        });
    }


}
